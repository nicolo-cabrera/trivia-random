import React,{ useCallback, useEffect, useRef, useState } from 'react'
import { Button } from 'react-bootstrap';
import ReactCanvasConfetti from "react-canvas-confetti";
import styles from '../CSS/Draw.module.css'
import swal from 'sweetalert'
import Swal from 'sweetalert2'



const Draw = () => {

    const [prize, setPrize] = useState({})
    const [result, setResult] = useState(false)
    const [start, setStart] = useState(false)
    const [loading, setLoading] = useState(false)
    const [counter, setCounter] = useState([])
    const [ background, setBackground ] = useState("")

    let applause = new Audio("/sound/applause.mp3")

    useEffect(() => {

        document.title = `Draw`
        fetchBackground()
        fetchAllData()

    }, [])

    const fetchAllData = async () => {

      const res = await fetch(`https://powerful-mesa-13293.herokuapp.com/api/prize/all`)
      const data = await res.json()
      setCounter(data)

  } 

  const fetchBackground = async () => {

    const res = await fetch(`https://powerful-mesa-13293.herokuapp.com/api/image/background`)
    const data = await res.json()
    setBackground(data[0].path_name)


  }
  

    const refAnimationInstance = useRef(null);

    const getInstance = useCallback((instance) => {

        refAnimationInstance.current = instance;

      }, []);

      const makeShot = useCallback((particleRatio, opts) => {

        refAnimationInstance.current &&
          refAnimationInstance.current({
            ...opts,
            origin: { y: 0.7 },
            particleCount: Math.floor(200 * particleRatio)
          });

      }, []);

      const fire = useCallback(() => {

        makeShot(0.25, {

          spread: 26,
          startVelocity: 55

        });
    
        makeShot(0.2, {

          spread: 60

        });
    
        makeShot(0.35, {

          spread: 100,
          decay: 0.91,
          scalar: 0.8

        });
    
        makeShot(0.1, {

          spread: 120,
          startVelocity: 25,
          decay: 0.92,
          scalar: 1.2

        });
    
        makeShot(0.1, {

          spread: 120,
          startVelocity: 45

        });

      }, [makeShot]);
 

      const canvasStyles = {

        position: "fixed",
        pointerEvents: "none",
        width: "100%",
        height: "100%",
        top: 0,
        left: 0

      };


      const getPrize = () => {

        setLoading(true)
        setResult(false)
        setStart(true)

        fetch(`https://powerful-mesa-13293.herokuapp.com/api/prize/getRandomPrize`)
        .then(res => res.json())
        .then(result => {

            if(result !== false) {
            
              
              setLoading(false)
              setPrize(result)
              setResult(true)
              fetchAllData()
              fire()

              applause.play()

              Swal.fire({

                text: `${result.prize_name}`,
                width: 600,
                padding: '3em',
                imageUrl: `${process.env.PUBLIC_URL}/${result._id}.jpg`,
                imageAlt: 'Prize'

              })

                
            } else {

                setStart(false)

                swal({

                    title: "There are no prizes left.",
                    icon: "error",
                    button: "Close",

                  });
            }
        })
        .catch(err => {

            console.log(err)
        })
      }
    return (
        <>

        <div className={styles.container} style={{ 
            backgroundImage: `url("https://powerful-mesa-13293.herokuapp.com/controllers/client/public/uploads/${background}")` ,
           "background-repeat": "no-repeat",
           "center": "center fixed"
          }}>
            <div className={styles.roller}>
                {
                  start ? 
                  <>
                  {
                    result ?
                    <>
                    <h1>{prize.prize_name}</h1>
                    </>
                    :
                    <span id={styles.rolltext}>
                      {
                        counter.map(prize => {
                          return <>{prize.prize_name}<br/></>

                          
                        })
                      }
                    </span>
                  }
                  </>
                  :
                  <h1>Prize</h1>
                }
            </div>
          <div className={styles.button}>
            <Button onClick={() => getPrize()} disabled={loading}>{loading ? "Drawing Prize" : "Draw Prize"}</Button>
          </div>
          <div className={styles.counter}>
            {
              counter.map(prize => {
                return (
                  <>
                  <h5>{prize.prize_name}: {prize.qty}</h5>
                  </>
                )
              })
            }
          </div>
        </div>
        <ReactCanvasConfetti refConfetti={getInstance} style={canvasStyles} />
        
        </>
    );
}
 
export default Draw;