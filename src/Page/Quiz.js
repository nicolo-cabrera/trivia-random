import React, { useState, useEffect, useCallback, useRef } from 'react'
import ReactCanvasConfetti from "react-canvas-confetti";
import { Button } from 'react-bootstrap'
import styles from "../CSS/Quiz.module.css"
import Swal from 'sweetalert2'

const Quiz = () => {
    
    const [data, setData] = useState({})
    const [reveal, setReveal] = useState(false)
    const [finish, setFinish] = useState(false)
    const [gettingQuestion, setIsGettingQuestion] = useState(false)
    const [ background, setBackground ] = useState("")


    let applause = new Audio("/sound/applause.mp3")
    let sad = new Audio("/sound/sad.mp3")

    useEffect(() => {

        document.title = `Trivia`
        fetchBackground()

    }, [])

    // CONFETTI
    const refAnimationInstance = useRef(null);

    const getInstance = useCallback((instance) => {

        refAnimationInstance.current = instance;

      }, []);

      const makeShot = useCallback((particleRatio, opts) => {

        refAnimationInstance.current &&
          refAnimationInstance.current({
            ...opts,
            origin: { y: 0.7 },
            particleCount: Math.floor(200 * particleRatio)
          });

      }, []);

      const fire = useCallback(() => {

        makeShot(0.25, {
          spread: 26,
          startVelocity: 55

        });
    
        makeShot(0.2, {

          spread: 60

        });
    
        makeShot(0.35, {

          spread: 100,
          decay: 0.91,
          scalar: 0.8

        });
    
        makeShot(0.1, {

          spread: 120,
          startVelocity: 25,
          decay: 0.92,
          scalar: 1.2

        });
    
        makeShot(0.1, {

          spread: 120,
          startVelocity: 45

        });
      }, [makeShot]);
 

      const canvasStyles = {

        position: "fixed",
        pointerEvents: "none",
        width: "100%",
        height: "100%",
        top: 0,
        left: 0

      };
      //END OF CONFETTI

    const fetchBackground = async () => {

        const res = await fetch(`https://powerful-mesa-13293.herokuapp.com/api/image/background`)
        const data = await res.json()
        setBackground(data[0].path_name)

      }
    
    const getQuestion = (e) => {
      
        if(e.key === "Spacebar" || e.key === " ") {
            setIsGettingQuestion(true)
            setReveal(false)
            fetch(`https://powerful-mesa-13293.herokuapp.com/api/trivia/isaPangRandomTrivia`)
            .then(res => res.json())
            .then(result => {

                if(result !== false) {
                  
                    setData(result)
                    setIsGettingQuestion(false)
               
                }
            })
            .catch(err => {

                console.log(err)
            })
        }
        
    }

    const revealAnswer = () => {

        Swal.fire({

            title: 'Reveal the answer?',
            showDenyButton: true,
            showCancelButton: true,
            confirmButtonText: 'Ok',
            confirmButtonColor: '#0000ff',
            denyButtonColor: '#FFA500',
            denyButtonText: 'OK',
            customClass: {

              actions: 'my-actions',
              cancelButton: 'order-1',
              confirmButton: 'order-3',
              denyButton: 'order-2',

            }

          }).then((result) => {

            if (result.isConfirmed) {

                fire()  
                setReveal(true)
                applause.play()

            } else if (result.isDenied) {

                sad.play()
                Swal.fire({
                    imageUrl: `${process.env.PUBLIC_URL}/CRY.gif`,
                    imageAlt: 'Wrong answer'
                  })
            }
          })

    }
    

    
    return (  

        <div className={styles.container} style={{ 
            backgroundImage: `url("https://powerful-mesa-13293.herokuapp.com/controllers/client/public/uploads/${background}")` ,
           "backgroundRepeat": "no-repeat",
           "center": "center fixed"
          }}>
        {
            data.question === undefined ? 
            <>

                <div className={ finish ? styles.finish : styles.start}>
                    <p>{finish ? "There are no questions left." : "Press the space bar to start"}</p>
                </div>
                <Button onKeyUp={(e) => getQuestion(e)} autoFocus onBlur={e => e.currentTarget.focus()}> Get Question </Button>
                
            </>
            
            :
            <>
  
                <div className={styles.questioncontainer}>
                    <p onClick={() => revealAnswer()}>{data.question ? data.question : null}</p>
                </div>

                <div className={reveal ? styles.answercontainer : styles.answercontainerhidden}>
                    <p>{data.answer}</p>
                </div>
                <Button className={styles.button} onKeyUp={(e) => getQuestion(e)} autoFocus onBlur={e => e.currentTarget.focus()}> { gettingQuestion ? "Getting Question..." : "Get Question" }</Button>
                <ReactCanvasConfetti refConfetti={getInstance} style={canvasStyles} />

            </>
            
        }
        </div>

    );
}
 
export default Quiz;