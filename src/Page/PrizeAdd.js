import React, { useState, useEffect } from 'react'
import { Form, Button, Table, Nav } from 'react-bootstrap'
import { Link } from 'react-router-dom'
import swal from 'sweetalert'
import NavBar from '../Component/NavBar'
import styles from '../CSS/PrizeAdd.module.css'

const PrizeAdd = () => {

    const [prizeName, setPrizeName] = useState("")
    const [prizeQty, setPrizeQty] = useState(0)
    const [data, setData] = useState([])
    
    useEffect(() => {
        document.title = `Add Prize`
        fetchAllData()


    }, [])

    const fetchAllData = () => {

        fetch(`https://powerful-mesa-13293.herokuapp.com/api/prize/all`)
        .then(res => res.json())
        .then(data => {
            console.log(data);
            setData(data)
        })

    } 
    
    const submitForm = (e) => {

        e.preventDefault()

        const password = prompt("Password")

        if(password === "m1sd1t") {

                fetch(`https://powerful-mesa-13293.herokuapp.com/api/prize/addPrize`, {
                method: 'POST',
                headers: {

                'Content-Type': "application/json"
                
                },
                body: JSON.stringify({  
                    prize_name: prizeName,
                    qty: prizeQty

                })
            })
            .then(res => res.json())
            .then(data => {

                if(data) {
                    fetchAllData()
                    setPrizeName("")
                    setPrizeQty(0)  
                    swal({

                        title: "Success",
                        icon: "success",
                        button: "Close",

                    });

                } else {

                    swal({
                        title: "Error",
                        icon: "error",
                        button: "Close",
                    });
                }

            })

        } else {

            alert("Wrong password")
        }
        
    }

    const deletePrize = (id) => {

        const password = prompt("Password")

        if(password === "m1sd1t") {

            swal({
                title: "Delete question?",
                icon: "error",
                buttons:true,
                dangerMode: false,
            })
            .then(willReveal => {
                if (willReveal) {
    
                    fetch(`https://powerful-mesa-13293.herokuapp.com/api/prize/delete/${id}`, {
                        method: 'DELETE'
                    })
                    .then(res => res.json())
                    .then(data => {
                        if(data) {

                            swal({

                                title: "Success",
                                icon: "success",
                                button: "Close",

                              });

                              fetchAllData()

                        } else {
            
                            swal({

                                title: "Error",
                                icon: "error",
                                button: "Close",

                              });
                            
                        }
                        
                    })
                   
                }
               
            });

        } else {
            alert("Wrong password")
        }

    

    }


    return ( 
        <>
        
        <NavBar />
        <Form onSubmit={(e) => submitForm(e)} className={styles.form}>
            <Form.Group className="mb-3" controlId="formBasicQuestion">
                <Form.Label>Prize Name</Form.Label>
                <Form.Control type="text" placeholder="Prize Name" value={prizeName} onChange={e => setPrizeName(e.target.value)} required/>
                <Form.Text className="text-muted">
                Prize Name.
                </Form.Text>
            </Form.Group>

            <Form.Group className="mb-3" controlId="formBasicAnswer">
                <Form.Label>Quantity</Form.Label>
                <Form.Control type="number" placeholder="Enter Answer" value={prizeQty} onChange={e => setPrizeQty(e.target.value)} required min="0"/>
                <Form.Text className="text-muted">
                Quantity.
                </Form.Text>
            </Form.Group>

            <Button variant="primary" type="submit">
                Submit
            </Button>
        </Form>

        {
            data.length < 1 ?
            <h1>Loading data...</h1>
            :
            <Table striped bordered hover size="sm">
            <thead>
                <tr>
                    <th>Prize Name</th>
                    <th>Quantity</th>
                    <th>Edit</th>
                    <th>Delete</th>
                </tr>
            </thead>
            <tbody>
                {
                    data.map(prize => {
                        return (
                                <tr key={`prize ${prize._id}`}>
                                    <td>{prize.prize_name}</td>
                                    <td>{prize.qty}</td>
                                    <td><Link to={`/prize/${prize._id}`}><Button variant='success'>Edit</Button></Link></td>
                                    <td><Button variant='danger' onClick={() => deletePrize(prize._id)}>Delete</Button></td>
                                </tr>

                        )
                    })
                }
            </tbody>
        </Table>

        }
    
        </>
     );
}
 
export default PrizeAdd;