import React, { useState, useEffect }from "react";
import { Form, Button, Table } from 'react-bootstrap'
import { Link } from "react-router-dom";
import swal from 'sweetalert'
import styles from "../CSS/Add.module.css"
import NavBar from "../Component/NavBar";


const Add = () => {

    const [ question, setQuestion ] = useState("")
    const [ answer, setAnswer ] = useState("")
    const [data, setData] = useState([])


    useEffect(() => {
        document.title = `Add Trivia`
        fetchAllData()
    }, [])

    const submitForm = (e) => {
        e.preventDefault()

        const password = prompt("Password") 

        if(password === "m1sd1t") {
            if(question === "" && answer ==="") {
                swal({
                    title: "Error",
                    icon: "error",
                    button: "Close",
                  });
            }
            
           
            fetch(`https://powerful-mesa-13293.herokuapp.com/api/trivia/add`, {
                method: 'POST',
                headers: {
    
                   'Content-Type': "application/json"
                   
                },
                body: JSON.stringify({  
                    question: question,
                    answer: answer
    
                })
            })
            .then(res => res.json())
            .then(data => {
                if(data) {
    
                    setQuestion("")
                    setAnswer("")
                    fetchAllData()
    
                    swal({

                        title: "Success",
                        icon: "success",
                        button: "Close",

                      });

                } else {

                    swal({

                        title: "Error",
                        icon: "error",
                        button: "Close",
                        
                      });
                }
    
            })
    
        } else {

            alert("Wrong password")

        }
        
    }

    const fetchAllData = () => {
        
        fetch(`https://powerful-mesa-13293.herokuapp.com/api/trivia/all`)
        .then(res => res.json())
        .then(data => {

            console.log(data);
            setData(data)
        })
    } 


    const deleteTrivia = (id) => {

        const password = prompt("Password") 

        if(password === "m1sd1t") {

            swal({

                title: "Delete question?",
                icon: "error",
                buttons:true,
                dangerMode: false,
            })
            .then(willReveal => {

                if (willReveal) {
    
                    fetch(`https://powerful-mesa-13293.herokuapp.com/api/trivia/delete/${id}`, {
                        method: 'DELETE'
                    })
                    .then(res => res.json())
                    .then(data => {

                        if(data) {

                            swal({

                                title: "Success",
                                icon: "success",
                                button: "Close",

                              });

                        } else {
            
                            swal({

                                title: "Error",
                                icon: "error",
                                button: "Close",

                              });
                            
                        }
                        fetchAllData()
                    })
                   
                }
               
            });

        } else {

            alert("Wrong password")

        }

        
    }

    
    const resetQuestion = () => {

        const password = prompt("Password") 

        if(password === "m1sd1t") {

            swal({

                title: "Reset all question?",
                icon: "warning",
                buttons:true,
                dangerMode: false,

            })
            .then(willReveal => {

                if (willReveal) {
    
                    fetch("https://powerful-mesa-13293.herokuapp.com/api/trivia/resetTrivia")
                    .then(res => res.json())
                    .then(data => {
                        swal({
                            title: "Success",
                            icon: "success",
                            button: "Close",
                          });
                        fetchAllData()
                    })
                   
                }
               
            });

        } else {

            alert("Wrong password")

        }
        

    }
    


    return (  

        <>

        <NavBar />
            <Form onSubmit={(e) => submitForm(e)}>
                <Form.Group className="mb-3" controlId="formBasicQuestion">
                    <Form.Label>Question</Form.Label>
                    <Form.Control type="text" placeholder="Enter Question" value={question} onChange={e => setQuestion(e.target.value)} required/>
                    <Form.Text className="text-muted">
                    Question.
                    </Form.Text>
                </Form.Group>

                <Form.Group className="mb-3" controlId="formBasicAnswer">
                    <Form.Label>Answer</Form.Label>
                    <Form.Control type="text" placeholder="Enter Answer" value={answer} onChange={e => setAnswer(e.target.value)} required/>
                    <Form.Text className="text-muted">
                    Answer.
                    </Form.Text>
                </Form.Group>

                <Button variant="primary" type="submit">
                    Submit
                </Button>
            </Form>
            <div className={styles.reset}>
                <Button onClick={() => resetQuestion()}>{`Reset all ${data.length} Questions`}</Button>
            </div>

            {
                data.length < 1 
                ?
                <h1>Loading data...</h1>
                :
                <Table striped bordered hover size="sm" responsive>
                <thead>
                    <tr>
                        <th>Question</th>
                        <th>Answer</th>
                        <th>Done?</th>
                        <th>Edit</th>
                        <th>Delete</th>
                    </tr>
                </thead>
                <tbody>
                    {
                        data.map(trivia => {
                            return (
                                    <tr key={`trivia ${trivia._id}`}>
                                        <td>{trivia.question}</td>
                                        <td>{trivia.answer}</td>
                                        <td>{trivia.status ? "Yes" : "No"}</td>
                                        <td><Link to={`/trivia/${trivia._id}`}><Button variant='success'>Edit</Button></Link></td>
                                        <td><Button variant='danger' onClick={() => deleteTrivia(trivia._id)}>Delete</Button></td>
                                    </tr>

                            )
                        })
                    }
                </tbody>
            </Table>
              
            }
        
        </>

    );
}
 
export default Add;