import React, { useState } from "react";
import styles from '../CSS/AddImage.module.css'
import NavBar from "../Component/NavBar";
import swal from "sweetalert";

const AddImage = () => {

    const [selectedFile, setSelectedFile] = useState([]);

    const handleSubmission = () => {

        const data = new FormData()
        data.append('files', selectedFile)
        console.log(selectedFile);
       fetch("https://powerful-mesa-13293.herokuapp.com/api/image/create", {
            method: "POST",
            body: data
       })
       .then(res => res.json(res))
       .then(data => {

        if(data) {
            swal({
                title: "Success",
                icon: "success",
                button: "Close",
              });
        } else {

            swal({
                title: "Error",
                icon: "error",
                button: "Close",
              });
        }
           
       })
    
    }
    return ( 
        <>
        
        <NavBar />
        <div className={styles.container}>
            <div>

            <input type="file" name="file" onChange={e => setSelectedFile(e.target.files[0])}/> 
            <button onClick={() => handleSubmission()}>Submit</button>
            </div>
        </div>
        </>

    
    
    );
}
 
export default AddImage;