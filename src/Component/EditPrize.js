import React, { useState, useEffect } from "react";
import { useParams, Link } from "react-router-dom"
import { Form, Button } from 'react-bootstrap'
import swal from 'sweetalert'
import NavBar from "./NavBar";



const EditPrize = () => {

    const params = useParams()
    const id = params.id


    const [prizeName, setPrizeName] = useState("")
    const [prizeQty, setPrizeQty] = useState(0)

    useEffect(() => {
        document.title = `Edit Prize | ${id}`

        fetch(`https://powerful-mesa-13293.herokuapp.com/api/prize/prize/${id}`)
        .then(res => res.json())
        .then(data => {
            console.log(data);
            setPrizeName(data.prize_name)
            setPrizeQty(data.qty)
        })
        
    },[])

    const submitForm = (e) => {
        e.preventDefault()
        const password = prompt("Password")

        if(password === "m1sd1t") {
            fetch(`https://powerful-mesa-13293.herokuapp.com/api/prize/editPrize/${id}`, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json'
            },

            body: JSON.stringify({
                prize_name: prizeName,
                qty: prizeQty
            })
            
        })
        .then(res => res.json())
        .then(data => {
            if(data) {
                swal({
                    title: "Success",
                    icon: "success",
                    button: "Close",
                  });
            }
        })

        } else {
            alert("Wrong password")
        }
        
        
    }
    
    return (
            <>
                <NavBar />
                <Form onSubmit={(e) => submitForm(e)}>
                    <Form.Group className="mb-3" controlId="formBasicQuestion">
                        <Form.Label>Prize Name</Form.Label>
                        <Form.Control type="text" placeholder="Enter Question" value={prizeName} onChange={e => setPrizeName(e.target.value)} required/>
                        <Form.Text className="text-muted">Question.</Form.Text>
                    </Form.Group>

                    <Form.Group className="mb-3" controlId="formBasicAnswer">
                        <Form.Label>Quantity</Form.Label>
                        <Form.Control type="number" placeholder="Enter Answer" value={prizeQty} onChange={e => setPrizeQty(e.target.value)} required min="0"/>
                        <Form.Text className="text-muted">Answer.</Form.Text>
                    </Form.Group>
                    <Button variant="primary" type="submit">Submit</Button>
                    <Link to={`/addprize`}><Button variant='success'>Go Back</Button></Link>
                </Form>
            </>
        
        );
}
 
export default EditPrize;