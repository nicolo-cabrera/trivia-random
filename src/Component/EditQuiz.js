import React, { useState, useEffect } from "react";
import { useParams, Link } from "react-router-dom"
import { Form, Button} from 'react-bootstrap'
import swal from 'sweetalert'
import NavBar from "./NavBar";



const EditPrize = () => {

    const params = useParams()
    const id = params.id


    const [ question, setQuestion ] = useState("")
    const [ answer, setAnswer ] = useState("")
    const [ status, setStatus ] = useState(false)

    useEffect(() => {
        document.title = `Edit Quiz | ${id}`
        fetch(`https://powerful-mesa-13293.herokuapp.com/api/trivia/trivia/${id}`)
        .then(res => res.json())
        .then(data => {
            console.log(data);
            setQuestion(data.question)
            setAnswer(data.answer)
            setStatus(data.status)
        })
        
    },[])

    

    const submitForm = (e) => {
        e.preventDefault()

        const password = prompt("Password")

        if(password === "m1sd1t") {
                fetch(`https://powerful-mesa-13293.herokuapp.com/api/trivia/editTrivia/${id}`, {
                method: 'PUT',
                headers: {
                    'Content-Type': 'application/json'
                },

                body: JSON.stringify({
                    question: question,
                    answer: answer,
                    status: status
                })
                
            })
            .then(res => res.json())
            .then(data => {
                if(data) {
                    swal({
                        title: "Success",
                        icon: "success",
                        button: "Close",
                    });
                }
            })
        } else {
            alert("Wrong Password")
        }
        


        
    }

    const handleSelect = (e) => {
        const value = JSON.parse(e.target.value)
        setStatus(value)

    }
    
    console.log(status)
    return (
        <>
            <NavBar />
            <Form onSubmit={(e) => submitForm(e)}>
                <Form.Group className="mb-3" controlId="formBasicQuestion">
                    <Form.Label>Question</Form.Label>
                    <Form.Control type="text" placeholder="Enter Question" value={question} onChange={e => setQuestion(e.target.value)} required/>
                    <Form.Text className="text-muted">
                    Question.
                    </Form.Text>
                </Form.Group>

                <Form.Group className="mb-3" controlId="formBasicAnswer">
                    <Form.Label>Answer</Form.Label>
                    <Form.Control type="text" placeholder="Enter Answer" value={answer} onChange={e => setAnswer(e.target.value)} required/>
                    <Form.Text className="text-muted">
                    Answer.
                    </Form.Text>
                </Form.Group>

                <Form.Group className="mb-3" controlId="formBasicStatus">
                <Form.Label>Status</Form.Label>
                    <Form.Select aria-label="Default select example" onChange={e => handleSelect(e)} defaultValue={status} value={status} required>
                    <option value={true}>True</option>
                    <option value={false}>False</option>
                    </Form.Select>
                    <Form.Text className="text-muted">
                    Status.
                    </Form.Text>
                </Form.Group>

                <Button variant="primary" type="submit">
                    Submit
                </Button>
                <Link to={`/`}><Button variant='success'>Go Back</Button></Link>
            </Form>
        </>
        
        
        
        );
}
 
export default EditPrize;