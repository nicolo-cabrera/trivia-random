import React from 'react'
import { Navbar, Nav, Container, NavDropdown } from 'react-bootstrap';
import { NavLink } from 'react-router-dom';

const NavBar = () => {
    return (  

    <Navbar bg="light" expand="lg" >
    <Container>
        <Navbar.Brand href="#home">VIVAMAX</Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
        <Nav className="m-auto">
            <NavLink to="/trivia" target="_blank" className="nav-link">Trivia Page</NavLink>
            <NavLink to="/draw" target="_blank"     className="nav-link">Draw Page</NavLink>
            <NavLink to="/" className="nav-link">Add Trivia Page</NavLink>
            <NavLink to="/addprize" className="nav-link">Add Prize Page</NavLink>
            <NavLink to="/image" className="nav-link">Add Image Background</NavLink>
        </Nav>
        </Navbar.Collapse>
    </Container>
    </Navbar>   
    );
}
 
export default NavBar;
