import {Routes ,Route} from 'react-router-dom'

import Quiz from './Page/Quiz';
import Add from './Page/Add';
import Draw from './Page/Draw'
import AddImage from './Page/AddImage';
import PrizeAdd from './Page/PrizeAdd';

import EditPrize from './Component/EditPrize';
import EditQuiz from './Component/EditQuiz'


function App() {

  return (


    <>

        <Routes>
          <Route path="/" element={<Add />}/>
          <Route path="/trivia" element={<Quiz />}/>
          <Route path="/draw" element={<Draw />}/>
          <Route path="/addprize" element={<PrizeAdd />}/>
          <Route path="/prize/:id" element={<EditPrize />}/>
          <Route path="/trivia/:id" element={<EditQuiz />}/>
          <Route path="/image" element={<AddImage />}/>
        </Routes>

    </>


  );

}

export default App;
